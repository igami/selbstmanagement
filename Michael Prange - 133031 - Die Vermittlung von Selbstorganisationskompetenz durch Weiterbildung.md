<h1>Die Vermittlung von Selbstorganisationskompetenz durch Weiterbildung</h1>

Hausarbeit Managementkompetenz  
vorgelegt von  
Michael Prange

Angefertigt im Studiengang Management  
für Ingenieur-und Naturwissenschaften  
an der Fachhochschule Bielefeld,  
Fachbereich Wirtschaft und Gesundheit

Sommersemester 2021

Erstprüfer: Prof. Dr. Bernd Helbich
Zweitprüfer: Prof. Dr. Volker Herzig

# Inhaltsverzeichnis

- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Tabellenverzeichnis](#tabellenverzeichnis)
- [1 Einleitung](#1-einleitung)
- [2 Selbstorganisationskompetenz in der Theorie](#2-selbstorganisationskompetenz-in-der-theorie)
  - [2.1 Was ist Selbstorganisationskompetenz?](#21-was-ist-selbstorganisationskompetenz)
  - [2.2 Warum ist das wichtig?](#22-warum-ist-das-wichtig)
  - [2.3 Welche Instrumente & Techniken gibt es?](#23-welche-instrumente--techniken-gibt-es)
- [3 Eine Weiterbildung muss ...](#3-eine-weiterbildung-muss-)
  - [3.1 ein Mindset setzen](#31-ein-mindset-setzen)
  - [3.2 verschiedene Techniken vermitteln](#32-verschiedene-techniken-vermitteln)
  - [3.3 praktische Anwendung der Techniken beinhalten und interaktiv sein](#33-praktische-anwendung-der-techniken-beinhalten-und-interaktiv-sein)
  - [3.4 nachhaltig sein und so Gewohnheiten etablieren](#34-nachhaltig-sein-und-so-gewohnheiten-etablieren)
- [4 Fazit](#4-fazit)
- [Literatur- und Quellenverzeichnis](#literatur--und-quellenverzeichnis)
- [Versicherung](#versicherung)

# Tabellenverzeichnis

# 1 Einleitung

Ziel dieser Hausarbeit ist es zu Prüfen ob die vier Annahmen aus der Präsentation notwendige Bedingungen für eine Weiterbildung zur Vermittlung von Selbstorganisationskompetenz sind.

Im zweiten Kapitel werden die Fragen, die bereits in der Präsentation beantwortet wurden, zusammengefasst:
- Was ist Selbstorganisationskompetenz?
- Warum ist das wichtig?
- Welche Instrumente & Techniken gibt es?

Im dritten Kapitel werden die vier Annahmen, aus der vorausgegangenen Präsentation, überprüft:
Eine Weiterbildung muss ...
- ein Mindset setzen
- verschiedene Techniken vermitteln
- praktische Anwendung der Techniken beinhalten und interaktiv sein
- nachhaltig sein und so Gewohnheiten etablieren

Im letzten Kapitel wird ein Fazit gezogen und zusammengefasst welche Kriterien eine Weiterbildung erfüllen muss, um Selbstorganisationskompetenz zu vermitteln.

# 2 Selbstorganisationskompetenz in der Theorie

## 2.1 Was ist Selbstorganisationskompetenz?

In der Literatur gibt es die Begriffe Selbstorganisation, Selbstmanagement, Zeitmanagement und Energiemanagement die nicht trennscharf verwendet werden.

  > "Selbstmanagementkompetenz umfasst die Bereitschaft und die Fähigkeit, das eigene Leben selbstverantwortlich zu steuern und so zu gestalten, dass Leistungsfähigkeit, Leistungsbereitschaft, Wohlbefinden und Balance gestärkt und langfristig erhalten werden. Selbstmanagement ist gelebte Selbstverantwortung." (Graf 2019, S. 12)

In älterer Literatur wird häufig von Zeitmanagement gesprochen und dieses als Synonym für Produktivität verwendet. Aktuelle Literatur erklärt hingegen, dass das klassische Zeitmanagement gescheitert ist und durch eine neues Zeitmanagement bzw. Energie- oder Selbstmanagement ersetzt werden muss. Die Zeit lässt sich nicht managen und es geht um mehr als das effiziente Erledigen von Aufgaben. Selbstmanagement ist als Lebensphilosophie zu verstehen, die einen Zustand mit ausreichend Kontrolle und Perspektive erreichen will. Ein gutes Selbstmanagement fördert das Wohlbefinden und die Balance zwischen den verschiedenen Lebensbereichen. Es unterstützt dabei die beste Leistung zielgerichtet abrufen zu können, damit neben dem Arbeiten genug Zeit und Energie für Familie, Freunde und Hobbys zur Verfügung steht.  
Der Begriff Arbeit muss im Kontext des Selbstmanagement erweitert definiert werden. Er umfasst, neben der Erwerbsarbeit, all die Tätigkeiten, die wir erledigen wollen und noch nicht erledigt haben. Dazu zählt Beispielsweise einen Urlaub zu planen. Das Selbstmanagement befasst sich damit bei der Arbeit die selbstgesetzten Resultate zu erzielen und mit voller Aufmerksamkeit und Energie zu handeln. Es unterstützt dabei zu erkennen was getan werden muss und welche Arbeiten langfristig gesehen nicht wertvoll genug sind um erledigt zu werden. Das Handeln soll sich an den eignen Werten orientieren und bewusst geschehen. Dadurch wird die Zufriedenheit nachhaltig und langfristig gesteigert.  
Selbstmanagement lässt sich nur in den Bereichen anwenden, in denen keine Fremdbestimmung erforderlich ist. Im Umkehrschluss bedeutet dies, dass mit Selbstmanagement auch die Verantwortung für das eigene Handeln und den eigenen Erfolg einher geht. Wird diese Verantwortung nicht wahrgenommen führt dies wieder zu einer Fremdbestimmung voller Unvorhersehbarkeiten. Auch In dieser Situation kann das klassische Zeitmanagement dabei helfen den Überblick zu behalten und Arbeit zeitnah und stressfrei zu erledigen. Dies führt meist jedoch nur dazu, dass die Aufgaben zwar schneller erledigt werden, sich aber keine Zufriedenheit einstellt, die Motivation sinkt und nicht das ganze Potenzial abgerufen werden kann.  
(vgl. Allen 2011, S. 66-69; vgl. Blatter 2019, S. 5-12, 24-27; vgl. Dietrich 2001, S. 39, 91; vgl. Graf 2019, S. 4, 11)

Zusammenfassend kann definiert werden, dass Selbstmanagement wichtig ist, wenn eigenständiges Handeln erforderlich ist. Dabei sollen die eigenen Werte erkannt werden, um so langfristige Ziele zu setzen, die Motivation hochzuhalten und die Zufriedenheit sicher zu stellen. Es geht um das bewusste Arbeiten an den wichtigen Aufgaben.  
Das klassische Zeitmanagement ist ein Teil des Selbstmanagement. Im klassischen Sinn liegt der Fokus bei der Bearbeitung von Aufgaben mit den Zielen einen Überblick zu behalten und die Produktivität zu steigern.  
In dieser Arbeit wird mit Selbstmanagement die Effektivität und mit Zeitmanagement die Effizienz verknüpft.  
Selbstorganisationskompetenz umfasst all die Fähigkeiten, die notwendig sind, um ein gutes Selbstmanagement zu ermöglichen.

## 2.2 Warum ist das wichtig?

Die Frage "Warum ist Selbstmanagement wichtig?" wurde indirekt im vorherigen Kapitel "Was ist Selbstorganisationskompetenz?" beantwortet und wird an dieser Stelle stichpunktartig zusammengefasst.

- Selbstmanagement ist sowohl beruflich als auch privat für alle Arbeiten anwendbar die nicht fremdbestimmt sind
- Selbstmanagement hilft die Kontrolle zu behalten und negative Folgen wie Stress und Hektik bis hin zu Burnout, Erschöpfungsdepression und Herzinfarkt zu vermeiden
- Selbstmanagement hilft Ziele zu definieren und durch effektive Arbeit diesen Zielen sicher näher zu kommen
- Selbstmanagement orientiert sich an den eigenen Werten und sorgt für bewusste Handlungen, wodurch die Zufriedenheit nachhaltig und langfristig gesteigert wird
- Selbstmanagement hilft fokussiert und effizient zu arbeiten
- Selbstmanagement steigert den Erfolg, da Arbeiten effektiv und effizient erledigt werden

## 2.3 Welche Instrumente & Techniken gibt es?

  > "A fool with a tool is still a fool." Grady Booch

Es gibt sehr viele Instrumente & Techniken, kurz Methoden genannt, die bei der Selbstorganisation unterstützen sollen und es kommen immer wieder neue hinzu bzw. werden weiterentwickelt. Zwischen der Fülle an Methoden gibt es viele Überschneidungen hinsichtlich der Ziele und Vorgehensweise. Tabelle 1 gibt eine Übersicht von bekannten Methoden mit Erläuterungen. Dabei handelt es sich jedoch lediglich um eine Auswahl, die keinesfalls den Anspruch hat vollständig zu sein. Die beeindruckende Fülle an Methoden soll nicht abschreckend wirken, sondern zeigt, im Gegenteil, dass Selbstmanagement ein höchst individuelles Thema ist. Selbstmanagement ist in erster Linie Kopfsache.  
Die angewandten Methoden müssen zu der eigenen Arbeitsweise passen. Nicht jede Methode ist für jede Person gleichermaßen geeignet und kann daher funktionieren – oder eben nicht. Ziel der Methoden ist es, dass wir unserer Aufgaben effektiver und effizienter erledigen können. Die Methoden sollen nicht zum Selbstzweck angewandt werden. Viele Methoden sind gerade deswegen simpel gehalten und einfach zu verstehen. Sie zu meistern erfordert jedoch einiges an Disziplin. Erst wenn die Ausführung einer Methode verinnerlicht wurde, darf damit begonnen werden diese abzuwandeln. Ansonsten kann es dazu kommen, dass sich falsche Muster einprägen und die Methode nicht die gewünschte Wirkung zeigt.  
(vgl. Blatter 2019, S. 146, 190f; vgl. Graf 2019, S. 234; vgl. Rudat 2019)

Die Methoden lassen sich auf verschiede Arten gruppieren. In dieser Arbeit wurde eine Gruppierung nach typischen Schritten des Zeitmanagement gewählt, wie sie Beispielsweise die ALPEN Methode nennt und zusätzlich mit den Meta-Themen des Selbstmanagement hinsichtlich Motivation und Perspektive erweitert. Bei Methoden mit einem Namen wird auf eine Beschreibung verzichtet, da es nicht Umfang dieser Hausarbeit ist die Methoden zu vermitteln. Neben den konkreten Methoden gibt es noch einige allgemein beschriebene Prinzipien.

Tab. 1: Übersicht von Selbstmanagement-Methoden nach Fokuspunkten gruppiert  
(Blatter 2018; Blatter 2019, S. 146-171; Blatter 2020; Blatter 2021; Graf 2019, S. 235f, 352; Grasemann 2021; Rudat 2019; Symhoven & Iu 2021; walter 2021; Wikipedia 2021; Windolph 2018; Windolph 2021)

| Fokus                                                                                                        | Methode                                                                                                                                                                                                                                                                                   |
| ------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Erfassen**</br>Arbeit sichtbar machen                                                                      | **Inbox zero**                                                                                                                                                                                                                                                                            |
|                                                                                                              | **ToDo Listen**                                                                                                                                                                                                                                                                           |
|                                                                                                              | **The One Minute To-do List**                                                                                                                                                                                                                                                             |
|                                                                                                              | **Personal Kanban**                                                                                                                                                                                                                                                                       |
| **Ziele setzen**</br>Definieren was zu erreichen ist                                                         | **Mit Zielen arbeiten**</br>Handlungswirksame berufliche und persönliche Ziele definieren, Umsetzungsschritte festlegen, Aktionsplan erstellen                                                                                                                                            |
|                                                                                                              | **Mindmapping**</br>Überblick über Ausgangssituation und Ziele verschaffen                                                                                                                                                                                                                |
|                                                                                                              | **Ziel-Mittel-Analyse**</br>Zur Verfügung stehende Mittel und Fähigkeiten sowie Vor- und Nachteile verschiedener Wege analysieren                                                                                                                                                         |
|                                                                                                              | **Pareto-Prinzip**                                                                                                                                                                                                                                                                        |
|                                                                                                              | **Parkinsonsche Gesetz**                                                                                                                                                                                                                                                                  |
| **Entscheidungen treffen**</br>Den Wert von einzelnen Arbeiten festlegen und Grundsatzentscheidungen treffen | **Eisenhower-Matrix**                                                                                                                                                                                                                                                                     |
|                                                                                                              | **ABC-Methode**                                                                                                                                                                                                                                                                           |
|                                                                                                              | **Eat the frog**                                                                                                                                                                                                                                                                          |
|                                                                                                              | **Ziel- und Zeitplanbuch**</br>Ziele und damit zusammenhängende Aufgaben festhalten, Aufgaben in die entsprechenden Pläne (Tages-, Wochenpläne etc.) übertragen auf der Basis des Prinzips: Wichtige Aufgaben haben Vorrang                                                               |
|                                                                                                              | **10-10-10**                                                                                                                                                                                                                                                                              |
|                                                                                                              | **High Value Activities**                                                                                                                                                                                                                                                                 |
|                                                                                                              | **Salami-Taktik**                                                                                                                                                                                                                                                                         |
|                                                                                                              | **Grenzen definieren, Nein sagen**</br>Grenzen des Machbaren erkennen, Grenzen kommunizieren und durchsetzen, z. B. zeitliches Limit für Besprechungen setzen und einhalten, Zuständigkeiten klären                                                                                       |
|                                                                                                              | **Not-To-Do-Liste**                                                                                                                                                                                                                                                                       |
|                                                                                                              | **Checklisten und Visualisierungen**</br>Listen mit den zu erledigenden Aufgaben erstellen oder mithilfe von Visualisierungen eine Übersicht über anstehende Aufgaben bzw. Prozessschritte ermöglichen (z. B. durch Kanban)                                                               |
|                                                                                                              | **Rituale**</br>Rituale benötigen, nach dem Erlernen und Installieren, kaum Anstrengung, sodass mehr Energie für andere Dinge zur Verfügung steht, die nicht routinemäßig abzuarbeiten sind                                                                                               |
|                                                                                                              | **Bündelung**</br>Gleichartige Arbeiten zusammenfassen, damit diese in einem Schritt erledigt werden können (z. B. E-Mail-Bearbeitung, Telefonate)                                                                                                                                        |
| **Planen**</br>Tages-, Wochen-, Monats-, Quartals- und Jahrespläne erstellen                                 | **Timeboxing**                                                                                                                                                                                                                                                                            |
|                                                                                                              | **Zeitfenster**</br>Zeitfenster für Aufgaben, die Konzentration erfordern, oder für die E-Mail-Bearbeitung definieren                                                                                                                                                                     |
|                                                                                                              | **ALPEN-Methode**                                                                                                                                                                                                                                                                         |
|                                                                                                              | **25.000-Dollar-Methode**                                                                                                                                                                                                                                                                 |
|                                                                                                              | **Getting Things Done (GTD)**                                                                                                                                                                                                                                                             |
|                                                                                                              | **Besprechungen effektiv und effizient gestalten**</br>Sitzungen und Besprechungen vorbereiten, effektiv und effizient gestalten, Zeiten einhalten                                                                                                                                        |
| **Fokussiert arbeiten**</br>Den Plan effektiv umsetzen                                                       | **60-60-30**                                                                                                                                                                                                                                                                              |
|                                                                                                              | **Pomodoro-Technik**                                                                                                                                                                                                                                                                      |
|                                                                                                              | **Delegieren**</br>Aufgaben oder Teilaufgaben und die dafür notwendige Handlungskompetenz weitergeben mit dem Ziel, sich zu entlasten                                                                                                                                                     |
|                                                                                                              | **Leistungskurve/Biorhythmus**</br>Tag auf der Basis der persönlichen Leistungskurve gestalten                                                                                                                                                                                            |
|                                                                                                              | **Goldene Stunde**</br>Für eine Stunde pro Tag sämtliche Störungen ausschalten (keine Telefonate und E-Mails, Bürotür schließen oder Symbol für "keine Störung" aufs Pult stellen)                                                                                                        |
|                                                                                                              | **Störungen, Unterbrechungen, Zeitdiebe reduzieren**</br>Telefon umleiten, feste Sprechzeiten einplanen, Multitasking einschränken                                                                                                                                                        |
| **Reflektieren**</br>Das eigene Handeln hinterfragen und anpassen                                            | **Erfolgskontrolle**</br>Tägliche Erfolgskontrolle kann sehr motivierend sein, z. B. Abhaken von Aufgaben auf der Liste                                                                                                                                                                   |
|                                                                                                              | **Unterstützung** frühzeitig suchen und annehmen                                                                                                                                                                                                                                          |
|                                                                                                              | **Ablagesystem optimieren**</br>Übersichtliches Ablage- und Archivierungssystem erstellen (physisch und in Outlook)                                                                                                                                                                       |
|                                                                                                              | **Rubber Ducking**</br>Ursprünglich eine Methode zum Debuggen von Computerprogrammen, die jedoch auch auf andere Bereiche übertragen werden, kann. Dabei wird ein Sachverhalt kleinteilig einem Quietscheentchen erklärt, um so einen Blick von außen zu bekommen                         |
|                                                                                                              | **Mastermind-Gruppe**                                                                                                                                                                                                                                                                     |
| **Motivieren**</br>Herausfinden was die eigenen Werte sind, die uns motivieren                               | **Erfolgserlebnisse**</br>Erfolge genießen und feiern                                                                                                                                                                                                                                     |
|                                                                                                              | **Selbstwirksamkeit stärken**</br>Im Sinne von: Ich kann die Aufgaben und Probleme bewältigen und wenn nötig delegieren                                                                                                                                                                   |
|                                                                                                              | **Der Weg zum Wesentlichen**</br>Lebensphilosophie entwickeln, Lebensrollen bestimmen, Qualitätsziele für die verschiedenen Rollen definieren und in die Planung einfließen lassen                                                                                                        |
|                                                                                                              | **Vorbilder**</br>Es ist hilfreich, Vorbilder zu haben, die das gewünschte Verhalten zeigen. Wichtig ist dabei, diejenigen Verhaltensweisen zu übernehmen, die auch passen                                                                                                                |
| **Methoden lernen**                                                                                          | **Shu Ha Ri – Lernen in Stufen**</br>                                                                                                                                                                                                                                                     |
|                                                                                                              | **Jerry-Seinfeld-Methode**                                                                                                                                                                                                                                                                |
|                                                                                                              | **Podcasts**</br>Um neue Methoden zu erlernen eignen sich auch Podcasts wie beispielsweise</br>- Ivan Blatter: einfach produktiv</br>- David Symhoven & De Long Iu: Wir müssen reden!</br>- Jörg Walter: Projektmanagement im Maschinenbau</br>- Andrea Windolph: Projekte leicht gemacht |

# 3 Eine Weiterbildung muss ...

In diesem Kapitel werden die Aussagen aus der Präsentation anhand der Informationen aus dem vorherigen Kapitel überprüft.

## 3.1 ein Mindset setzen

  > Alice:  "Würdest du mir bitte sagen, welchen Weg ich einschlagen muss?"  
  > Grinsekatze:  "Das hängt in beträchtlichem Maße davon ab, wohin du gehen willst."  
  > Alice:  "Oh, das ist mir ziemlich gleichgültig."  
  > Grinsekatze:  "Dann ist es auch einerlei, welchen Weg du einschlägst."  
  > Alice:  "Hauptsache, ich komme irgendwohin."  
  > Grinsekatze:  "Das wirst du sicher, wenn du lange genug gehst."  
  > (Lewis Carroll, "Alice im Wunderland")

Wie in Kapitel 2.1 und 2.3 dargestellt ist Selbstmanagement ein sehr individuelles Thema und benötigt, vor allen Methoden, die richtige Einstellung.  
Eine Weiterbildung sollte klarstellen, dass es sich bei Selbstmanagement um einen langfristigen Prozess der Persönlichkeitsentwicklung handelt auf den es sich einzulassen gilt. Es ist nicht damit getan verschiedene Methoden anzuwenden, um bei der Erwerbsarbeit mehr zu leisten. Selbstmanagement hat das Ziel die Zufriedenheit nachhaltig zu steigern und dabei auch auf die physische und psychische Gesundheit zu achten. Es geht darum eine Lebensphilosophie zu entwickeln, die eigenen Rollen zu erkennen, Ziele für die Rollen festzulegen und diese zu erreichen. Dafür ist es erforderlich die eigene Komfortzone zu verlassen, sich dem eigenen Handeln bewusst zu werden und sich selbst auf dem Weg zu den Zielen immer wieder anzupassen.  
Der erste Schritt bei diesem Prozess der Persönlichkeitsentwicklung ist die Selbsterkenntnis. Damit werden die eigenen Werte, grundsätzlichen Prinzipien, Überzeugungen und Grenzen ermittelt. Aus diesen lassen sich anschließen Ziele von zwei Seiten her ableiten: 1. Was soll erreicht werden? 2. Was soll vermieden werden? Weiterhin muss der aktuelle Zustand und die eigenen Kompetenzen ermittelt werden. Dies bedeutet auch, sich selbst Schwächen einzugestehen und unbefriedigende Situationen als solche zu erkennen.  
Neben der Selbsterkenntnis ist die Selbstverantwortung grundlegend für ein gutes Selbstmanagement. Jede Person ist selbst für das eigene Handeln und die eigene Entwicklung verantwortlich. Wird diese Verantwortung nicht wahrgenommen kommt es zur Fremdbestimmung. Auch das kann bewusst angenommen werden, nur ist dann ein Selbstmanagement, zumindest in diesem Bereich, nicht mehr möglich und es liegt an anderen Personen, ob die gewollten Ziele erreicht werden. Selbstverantwortung zu übernehmen, bedeutet auch fokussiert zu sein, gute Gewohnheiten zu etablieren und die passenden Methoden zu verwenden. Nur weil etwas in der Theorie funktioniert und von anderen in der Praxis angewendet wird bedeutet dies nicht, dass es für alle gleichermaßen funktioniert. Es muss selbst die Verantwortung dafür übernommen werden zu erkennen was unterstützt und was Ballast ist, anstelle die Schuld für das Scheitern bei Anderen oder der Methode zu suchen.  
(vgl. Allen 2011, S. 38f; vgl. Blatter 2019, S. 25-30, 39f, 48, 58-61, 173f, 191; vgl. Graf 2019, S. 236, 379-383)

Zusammenfassend kann gesagt werden, dass Selbstmanagement zuerst im Kopf stattfinden muss. Nur die Personen die bereit sind sich selbst zu erkennen, Ziele zu setzen, die Verantwortung für das Handeln auf dem Weg der eigenen Entwicklung zu übernehmen und sich auf einen langfristigen Prozess einzustellen können Selbstmanagement verstehen und erfolgreich anwenden. Dadurch lässt sich das eigene Leben so steuern, dass Leistungsfähigkeit, Leistungsbereitschaft, Wohlbefinden und Balance gestärkt und langfristig erhalten werden. Methoden helfen dabei die Ziele zu erreichen. Selbstmanagement ohne eigene Ziele ist klassisches Zeitmanagement, das zum Selbstzweck oder für die Ziele anderer ausgeführt wird.  
Selbstmanagement ist sicherlich nicht für jede Person geeignet. Dies sollte bewusst erkannt und die Fremdbestimmung angenommen werden.  

## 3.2 verschiedene Techniken vermitteln

Wie in Kapitel 2.3 dargestellt gibt es viele verschiedene Methoden mit unterschiedlichen Fokuspunkten, die bei der Selbstorganisation unterstützen sollen.  
Aus all den Methoden muss ein persönlicher Werkzeugkoffer zusammengestellt werden, der noch handhabbar ist und trotzdem alles beinhaltet was benötigt wird. Um die Methoden für eine Person in "funktioniert für mich" und "funktioniert nicht für mich" einzuteilen müssen die Methoden erst bekannt sein. Sonst kann es dazu kommen, dass aus Mangel an Alternativen ein "funktioniert nicht für mich" angewendet wird, welche sich negativ auswirken kann.  
Um eine Vorauswahl der Methoden zu ermöglichen können die Arbeitsweisen der Personen analysiert werden, die an der Weiterbildung teilnehmen. Dies ist zugleich auch ein Teil der Selbsterkenntnis und bedient damit eine wichtige Voraussetzung des Selbstmanagement. Nur was außerhalb des Kopfes erfasst und sichtbar gemacht wurde kann effektiv bearbeitet werden.  
Die vorausgewählten Methoden müssen anschließend vorgestellt und die teilweise kleinen Unterschiede hervorgehoben werden - sind sich doch 60-60-30 und Pomodoro im Ansatz sehr ähnlich. Auch kann es sein, dass manche Methoden für Anfänger des Selbstmanagement geeignet sind und auf dem Weg zu den Zielen durch andere Methoden ausgewechselt werden müssen. Pauschal gilt, dass simple Methoden komplexen Methoden vorzuziehen sind. Mit zunehmendem Verständnis kann ein verzahntes, komplexes System jedoch mehreren simplen und unabhängigen Methoden überlegen sein.  
Das Vermitteln von verschiedenen Techniken ist erforderlich um, wie im vorherigen Kapitel dargestellt, selbstverantwortlich die bestmögliche Auswahl treffen zu können.  
(vgl. Allen 2011, S. 14f; vgl. Blatter 2019, S. 27, 50, 59-77; vgl. Graf 2019, S. 390-393)

## 3.3 praktische Anwendung der Techniken beinhalten und interaktiv sein

Ebenfalls in Kapitel 2.3 wurde dargestellt, dass Methoden nicht für alle Personen gleichermaßen funktionieren. Die praktische und interaktive Anwendung von Methoden ist hilfreich, um zu erkennen ob die Methoden sich in der Praxis bewähren oder durch andere Methoden mit gleichem Fokus ausgetauscht werden müssen. Weiterhin kann so sichergestellt werden, dass die Methoden richtig angewendet werden und die gewünschte Wirkung zeigen. Gemäß Shu Ha Ri soll eine Methode erst vollständig verstanden sein und korrekt ausgeführt werden bevor damit begonnen werden kann sie abzuwandeln. Es ist daher Teil der Weiterbildung darauf zu achten. Dies kann auch interaktiv abgehalten werden, indem eine Gruppe analysiert welche verschiedenen Ausführungen es von der Methode gibt und wie es dazu gekommen ist. Am Ende ist es wichtig, dass die Methode Spaß bei der Ausführung bereitet und in der Praxis funktioniert, unabhängig von der Theorie. Dabei muss jedoch sichergestellt sein, dass diese Veränderung bewusst vorgenommen wurde.  
(vgl. Blatter 2019, S. 52, 60f; vgl. Rudat 2019)

## 3.4 nachhaltig sein und so Gewohnheiten etablieren

In Kapitel 2.1 und 3.1 wurde dargelegt, dass Selbstmanagement ein langfristiger Prozess ist.  
Basierend auf den Informationen der Selbsterkenntnis sollen sinnvolle Entscheidungen und Grundlagen getroffen werden. Dabei ist zu beachten, dass jede Entscheidung Willenskraft benötigt und es zur Ermüdung führt, wenn viele Entscheidungen zu treffen sind. Es ist Ratsam Grundsatzentscheidungen zu treffen, die nicht ohne Begründung geändert werden. Insbesondere am Anfang gibt es kein richtig oder falsch. Es geht darum zu erfahren was sich in der Praxis bewährt. Werden Entscheidungen ständig geändert führt dies dazu, dass bald keine Entscheidungen mehr selbst getroffen werden und sich wieder eine Fremdbestimmung einstellt oder Arbeiten aufgeschoben werden.  
Grundsätze bilden die Basis für Gewohnheiten. Jede Entscheidung verlangt, dass sich explizit mit der Fragestellung beschäftigt wird. In schwierigen oder chaotischen Situationen wird jedoch meist unterbewusst gehandelt. Genau für diese Situationen ist es wichtig gute Gewohnheiten zu etablieren und damit bereits bestehende, schlechte Gewohnheiten zu verdrängen. Gewohnheiten benötigen wenig, bis keine Aufmerksamkeit bei der Ausführung und sind damit eine der wichtigsten Methoden des Selbstmanagement, weil so mehr Aufmerksamkeit und Willenskraft für die wichtigen Aufgaben zur Verfügung steht, die sich nicht routinemäßig bearbeiten lassen. Eng verwandt mit den automatisch ablaufenden Gewohnheiten sind Rituale und Checklisten. Diese benötigen zwar Aufmerksamkeit bei der Bearbeitung, geben aber einen festen Ablauf vor und sorgen dadurch dafür, dass nichts vergessen wird. Insbesondere Rituale sollten dabei an den eigenen Rhythmus angepasst werden und die Energiekurve über den Tag effektiv ausnutzen.  
(vgl. Blatter 2019, S. 22, 32f, 55, 58, 173, 177, 187ff; vgl. Graf 2019, S. 169, 379ff)

Um gute Gewohnheiten zu erlernen benötigt es Grundsätze und Zeit. Wenn diese dann installiert sind, stellt sich automatisch eine nachhaltige Produktivitätssteigerung ein. Dies unterstützt bei dem langen Prozess des Selbstmanagement beständig besser zu werden. Mit jeder etablierten Gewohnheit gewinnen wir mehr Zeit um explizit Entscheidungen zu treffen.  
  > "Ausdauer schlägt heroische Einzelaktionen. Immer!" (Ivan Blatter)

# 4 Fazit

Durch Selbstmanagement lässt sich das eigene Leben so steuern, dass Leistungsfähigkeit, Leistungsbereitschaft, Wohlbefinden und Balance gestärkt und langfristig erhalten werden. Dazu ist es notwendig sich selbst zu erkennen, Ziele zu setzen, die Verantwortung für das Handeln auf dem Weg der eigenen Entwicklung zu übernehmen und sich auf einen langfristigen Prozess einzustellen.

  > "Selbstmanagement ist gelebte Selbstverantwortung." (Graf 2019, S. 12)

Für eine Weiterbildung besteht die Herausforderung darin die Teilnehmer auf ihrem jeweiligen Kenntnisstand abzuholen und auf dem Weg der Persönlichkeitsentwicklung zu begleiten. Eine einzige, unabhängige Weiterbildung zur Vermittlung von Selbstorganisationkompetenz ist nur schwer vorstellbar. Eher sollte es als eine Weiterbildungsreihe konzipiert werden und wie bei der Salamitaktik nacheinander einzelne Aspekte behandeln. Am Anfang die Klarstellung, dass es eine langfristige Persönlichkeitsentwicklung ist, auf die es sich einzulassen gilt, über die Hilfe zur Selbsterkenntnis und dem Definieren von Zielen zu den Methoden und Gewohnheiten.  
Auch sollte den Teilnehmern bewusst gemacht werden, dass es möglich ist Selbstmanagement nur für einige Bereiche anzuwenden und eine Mischung aus selbst und Fremdbestimmung zu leben, wie es für viele auch der Fall sein sollte.

Anstelle einer abschließenden Zusammenfassung soll an dieser Stelle der Horizont noch erweitert werden. Selbstmanagement hat viele Überschneidungen zu anderen Bereichen wie dem Projektmanagement, Scrum oder NLP (Neuro-Linguistisches Programmieren). Dort werden Methoden eingesetzt die abgewandelt auch für das Selbstmanagement vorstellbar sind. Weiterhin gibt es noch Sport oder Mediation, die ein Teil des Selbstmanagement sein können und sich auf die Entspannung, den oft unterschätzten, nicht produktiven Teil, beziehen. Es kann eine Bereicherung sein sich mit Personen aus diesen Bereichen darüber zu unterhalten und so neue Erfahrungen und Einblicke zu gewinnen.

# Literatur- und Quellenverzeichnis

|                                      |                                                                                                                                                                                                        |
| ------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Allen, David (2011)                  | Ich schaff das!: Selbstmanagement für den ­beruflichen und privaten Alltag, USA 2011                                                                                                                    |
| Blatter, Ivan (2018)                 | Rituale: Damit der Tag nicht aus dem Rahmen fällt, https://ivanblatter.com/rituale-rahmen/ (Zugriff: 22.08.2021)                                                                                       |
| Blatter, Ivan (2019)                 | Arbeite klüger - nicht härter!: So holen Sie das Beste aus Ihrer Zeit, ohne sich auszubeuten. Methoden und Tools für ein neues Zeitmanagement. Zeit optimal nutzen - Freiräume schaffen, Hannover 2011 |
| Blatter, Ivan (2020)                 | Mastermind: Einer für alle, alle für einen, https://ivanblatter.com/podcast/mastermind-gruppe/ (Zugriff: 22.08.2021)                                                                                   |
| Blatter, Ivan (2021)                 | einfach produktiv - der Podcast rund um das richtige Mindset, Zeitmanagement und Selbstmanagement, https://ivanblatter.com/zeitmanagement-podcast/ (Zugriff: 22.08.2021)                               |
| Dietrich, Andreas (2001)             | Selbstorganisation: Management Aus Ganzheitlicher Perspektive, Wiesbaden 2001                                                                                                                          |
| Graf, Anita (2019)                   | Selbstmanagementkompetenz in Organisationen stärken: Leistung, Wohlbefinden und Balance als Herausforderung, Wiesbaden 2019                                                                            |
| Grasemann, Kathrin (2021)            | Inbox-Zero – in 3 Schritten zum leeren Posteingang & freiem Kopf, https://onlinebusinessakademie.net/inbox-zero/ (Zugriff: 22.08.2021)                                                                 |
| Rudat, Anna (2019)                   | Shu Ha Ri - Lernen in Stufen, https://www.wibas.com/blog/shu-ha-ri/ (Zugriff: 22.08.2021)                                                                                                              |
| Symhoven, David & Iu, De Long (2021) | Wir müssen reden! - Der SCRUM & NLP Podcast, https://wirmuessenreden.podigee.io/ (Zugriff: 22.08.2021)                                                                                                 |
| Walter, Jörg (2021)                  | Projektmanagement im Maschinenbau, https://www.projektmanagement-maschinenbau.de/podcast/ (Zugriff: 22.08.2021)                                                                                        |
| Wikipedia (2021)                     | Quietscheentchen-Debugging, https://de.wikipedia.org/w/index.php?title=Quietscheentchen-Debugging&oldid=214304698 (Zugriff: 22.08.2021)                                                                |
| Windolph, Andrea (2018)              | Timeboxing: So wirst du produktiver!, https://projekte-leicht-gemacht.de/blog/pm-methoden-erklaert/timeboxing/ (Zugriff: 22.08.2021)                                                                   |
| Windolph, Andrea (2021)              | Projekte leicht gemacht - Podcast, https://projekte-leicht-gemacht.de/projektmanagement-podcast/ (Zugriff: 22.08.2021)                                                                                 |

# Versicherung

"Ich versichere, dass ich die vorstehende Arbeit selbständig angefertigt und mich fremder Hilfe nicht bedient habe. Alle Stellen, die wörtlich oder sinngemäß veröffentlichtem oder nicht veröffentlichtem Schrifttum entnommen sind, habe ich als solche kenntlich gemacht."

Unterschrift