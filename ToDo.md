

- [x] Was ist Selbstorganisationskompetenz?
- [x] Warum ist das Wichtig?
- [x] Welche Instrumente & Techniken gibt es?
- [ ] ein Mindset setzen
- [ ] verschiedene Techniken vermitteln
- [ ] praktische Anwendung der Techniken beinhalten und interaktiv sein
- [ ] nachhaltig sein und so Gewohnheiten etablieren
- [ ] Fazit