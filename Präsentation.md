<!-- Leiten Sie aus Ihrem Thema eine (oder mehrere) Frage(n) ab, welche zum Schluss der Hausarbeit beantwortet werden muss / müssen. Berücksichtigen Sie bei Ihren Überlegungen begründet ausgewählte Theorien und / oder Modelle, die für ihr Praxisbeispiel relevant erscheinen. Erarbeiten Sie sich auf dieser Basis einen eigenen theoretischen Standpunkt. Begründen Sie diesen Standpunkt. Beziehen Sie sich in einem **Praxisbeispiel** auf eine konkrete Person oder Gruppe oder Unternehmung, nehmen Sie die erforderliche objektive Distanz an. Beschreiben, analysieren und bewerten Sie die dargestellten Zusammenhänge. Klären Sie für sich die relevanten Begriffe, bleiben Sie bei einheitlichen Begriffen. Beziehen Sie die Theorie auf die Praxis (und umgekehrt), d.h. analysieren Sie das Praxisbeispiel auf „Folie“ der Theorie. Die im theoretischen Teil erörterten Sachverhalte sollten sich auch im Praxisteil wiederfinden. Geben Sie Handlungsempfehlungen für die Praxis ab. Durch welche Maßnahmen kann eine Steigerung / Verbesserung / Optimierung erzielt werden? Formulieren Sie ein Fazit und / oder einen Ausblick. Machen Sie eine Aussage, welche Ihrer Erkenntnisse noch eine Auswirkung auf die anfänglich dargestellte Theorie haben. -->

<!-- Erstellung eines Konzeptes zu der jeweiligen Hausarbeit in Form eines Exposés, in dem auf die Aufgaben- bzw. Problemstellung, die Zielsetzung und den Bearbeitungsgang in Form einer Gliederung sowie auf zentrale inhaltliche Aspekte eingegangen wird. Hierzu ist eine PowerPoint-Präsentation zu erstellen, die vom Verfasser am 03.07.2021 in einem Zoom-Meeting mit den anderen Kursmitgliedern des gleichen Betreuers im Umfang von ca. 15 Minuten gehalten wird. Die PowerPoint-Datei ist dem Betreuer spätestens bis zum 18.06.2021 per Mail (bernd.helbich@fh-bielefeld.de oder volker.herzig@fh-bielefeld.de) für die Bewertung und ggf. für Hinweise zur Überarbeitung zuzuleiten. -->


# Die Vermittlung von Selbstorganisationskompetenz durch Weiterbildung

Exposé zur Hausarbeit von Michael Prange  
wmi Managementkompetenz Sommersemester 2021

## Inhalt

 - [Was ist Selbstorganisationskompetenz?](#was-ist-selbstorganisationskompetenz)
 - [Warum ist das Wichtig?](#warum-ist-das-wichtig)
 - [Welche Instrumente & Techniken gibt es?](#welche-instrumente--techniken-gibt-es)
 - [Wie muss eine Weiterbildung gestaltet sein?](#wie-muss-eine-weiterbildung-gestaltet-sein)
 - [Fragen & Diskussion](#fragen--diskussion)

## Was ist Selbstorganisationskompetenz?

- Selbstorganisation / Selbstmanagement
  - autonomes Handeln
  - eigene Motivation erhöhen
  - Ziele definieren und erreichen
  - effektiv
- Zeitmanagement / Energiemanagement
  - Aufgaben schneller bearbeiten
  - Freizeit schaffen
  - effizient

## Warum ist das Wichtig?

- beruflich und privat anwendbar
- VUCA
  <!-- - "volatility" ("Volatilität") -->
  <!-- - "uncertainty" ("Unsicherheit") -->
  <!-- - "complexity" ("Komplexität") -->
  <!-- - "ambiguity" ("Mehrdeutigkeit") -->
- weniger Stress / Burnout vermeiden
  <!-- - Stress entsteht oft dann, wenn wir das Gefühl haben, die Kontrolle zu verlieren, mit den anstehenden Projekten nicht mehr fertig zu werden oder den Erwartungen nicht gerecht werden zu können. Wir werden hektisch und setzen uns selbst unter Druck, um alles zu schaffen. Selbstorganisation kann dafür sorgen, diesen Stress zu vermeiden, bevor er überhaupt entstehen kann. -->
  <!-- - Je organisierter Sie bei Ihrer Arbeit vorgehen, desto leichter wird es Ihnen fallen, den Überblick zu behalten, Prioritäten zu setzen und die richtigen Dinge zur richtigen Zeit anzugehen, um am Ende ganz in Ruhe alles zu schaffen. -->
  <!-- - Ein Spaziergänger trifft in einem Wald auf einen Holzfäller, der mühsam versucht, mit seiner stumpfen Axt einen Baum zu fällen. Er tritt an ihn heran und fragt “Aber guter Mann, Ihre Axt ist ja ganz stumpf. Warum schärfen Sie sie denn nicht?” Darauf antwortet der Arbeiter: “Dafür habe ich keine Zeit, ich muss doch den Baum fällen…!” -->
- Zielerreichung / Effektiv
  <!-- - Wer unorganisiert handelt und entscheidet, verrennt sich gerne einmal auf seinem Weg und kommt am Ende nicht mehr da an, wo er eigentlich hin wollte. So ist es auch im Job. Wenn Sie sich etwas vornehmen, dieses Ziel aber immer wieder aus den Augen verlieren, bleiben Sie am Ende auf der Strecke. -->
  <!-- - Mit der nötigen Selbstorganisation werden Sie jedoch gezielt auf Ihre Ziele hinarbeiten und diese am Ende auch erreichen – ganz gleich, ob Sie zwischenzeitlich vielleicht einen Umweg genommen haben. -->
- Zufriedenheit
- Produktivität / Effizient
  <!-- - Eine klare Ordnung und gute Organisation kann Ihnen dabei helfen, eine Menge Zeit zu sparen, da Sie unnötige Zeitfresser erkennen und beheben können. Selbstorganisation hilft außerdem dabei, anfallende Aufgaben schneller zu bearbeiten und so am Ende des Tages mehr Zeit für andere Dinge zur Verfügung zu haben. -->
  <!-- - Schließlich hat es wahrscheinlich jeder schon einmal erlebt, dass er Dinge einfach immer weiter vor sich hergeschoben hat, nur um kurz vor Schluss in Panik zu verfallen und dem Ärger zu erliegen, nicht rechtzeitig angefangen zu haben. -->
- Erfolgreicher
  <!-- - Selbstorganisation kann Ihnen dabei helfen, Aufgaben besser zu erledigen, produktiver und effizienter zu arbeiten. Das wird über kurz oder lang auch Ihr Vorgesetzter bemerken. Auf diese Weise kann sich Ihnen vielleicht die Chance bieten, die nächsten Karriereschritte zu gehen. -->
  <!-- - Außerdem führt Selbstorganisation oftmals zu mehr Eigeninitiative, da Sie sich und Ihre Fähigkeiten besser einschätzen können und diese aktiv nutzen wollen, um beruflich voran zu kommen. -->

## Welche Instrumente & Techniken gibt es?

- ToDo Listen / priorisieren / personal Kanban
  <!-- - dringend/wichtig (Eisenhauer-Matrix) -->
  <!-- - Protokollieren / Dokumentieren => wissen was passiert ist -->
- Timeboxing / Pomodoro
- Inbox zero / Benachrichtigungen abschalten
<!-- - Wechsel zwischen Aufgaben kostet Zeit -->
- Shu Ha Ri – Lernen in Stufen
- Rituale
- die richtigen Fragen stellen
- Ente / Mastermind Gruppe
- Vorbilder
<!-- - "Ein gutes neues Zeitmanagement beginnt nämlich nicht bei der Aufgaben­ liste, den Methoden oder Apps, sondern in Ihrem Kopf." Blatter (2019) S. 4 -->

## Wie muss eine Weiterbildung gestaltet sein?

- Ziel der Hausarbeit
- Mindset setzen
- verschiedene Techniken
- praktische Anwendung / interaktiv
- nachhaltig

# Fragen & Diskussion

michael.prange@fh-bielefeld.de  
codeberg.org/igami/selbstmanagement  
CC-BY-4.0

## Literatur

- David Allen: Getting Things Done. The Art of Stress-Free Productivity. Penguin Books, New York 2002.
- Roy F. Baumeister, Kathleen Vohs: Handbook of Self-Regulation, Research, Theory, and Applications. Guilford Press, New York 2004.
- Frederick H. Kanfer, Hans Reinecker, Dieter Schmelzer: Selbstmanagement-Therapie: Ein Lehrbuch für die klinische Praxis. 4. Auflage. Springer, Heidelberg 2006.
- Ivan Blatter: Arbeite Klüger - nicht härter. humboldt, Hannover 2019.
<!-- - Podcasts -->
<!-- - Wolfgang H. Staehle: Management. 7. Auflage. München 1994. -->
<!-- - Stephen R. Covey: Die 7 Wege zur Effektivität: Prinzipien für persönlichen und beruflichen Erfolg. 31. Auflage. GABAL, Offenbach 2014, ISBN 978-3-89749-573-9. -->
<!-- - Joseph P. Forgas u. a. (Hrsg.): Psychology of Self-Regulation. Psychology Press, New York 2009, ISBN 978-1-84872-842-4. -->
<!-- - Waldemar Pelz: Kompetent führen - Wirksam kommunizieren, Mitarbeiter motivieren. Gabler, Wiesbaden 2004, ISBN 3-409-12556-6. -->
<!-- - Mihály Csíkszentmihályi: Flow im Beruf. 2. Auflage. Klett-Cotta, Stuttgart 2004, ISBN 3-608-93532-0. -->
<!-- - https://www.vertriebslexikon.de/Selbstmanagement-Selbstorganisation.html -->
<!-- - https://de.wikipedia.org/wiki/Selbstmanagement -->
